package com.ingress.kafka.service;

import com.ingress.kafka.dto.UserDto;
import com.ingress.kafka.dto.UserResponseDto;
import com.ingress.kafka.dto.UserUpdateBalanceDto;
import com.ingress.kafka.model.User;
import com.ingress.kafka.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.modelmapper.ModelMapper;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final KafkaTemplate<String, Object> kafkaTemplate;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public void register(UserDto userDto) {
        userRepository.save(modelMapper.map(userDto, User.class));

        kafkaTemplate.send("user-register", userDto);
    }


    public UserResponseDto addBalance(Long id, UserUpdateBalanceDto dto) {
        //update balance
        User user = userRepository.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
        user.setBalance(user.getBalance() + dto.getBalance());
        userRepository.save(user);
        //message send to topic
        kafkaTemplate.send("user-update-balance", user);

        return modelMapper.map(user, UserResponseDto.class);
    }
}
