package com.ingress.kafka.controller;

import com.ingress.kafka.dto.UserDto;
import com.ingress.kafka.dto.UserResponseDto;
import com.ingress.kafka.dto.UserUpdateBalanceDto;
import com.ingress.kafka.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@Slf4j
public class UserController {

    private final UserService userService;

    @PostMapping("/register")
    public ResponseEntity<Void> publish(@RequestBody UserDto userDto) {
        userService.register(userDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/balance/{id}")
    public UserResponseDto addBalance(@PathVariable Long id, @RequestBody UserUpdateBalanceDto balance) {
        return userService.addBalance(id, balance);
    }
}
